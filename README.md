# LayTray

## About

This app keeps a small text icon in the notification area to let you know what Layout you are using. Developed for Blackberry KeyOne and possibly other QWERTY phones. This is based on the Toast notification that keyboard produces upon switching layouts.

An icon can help users with short-term memory problems or vision problems interact with their device.

## Get it  
[Play Store](https://play.google.com/store/apps/details?id=space.neothefox.laytray)  
[F-Droid](https://f-droid.org/en/packages/space.neothefox.laytray/)  

